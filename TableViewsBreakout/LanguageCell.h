//
//  LanguageCell.h
//  TableViewsBreakout
//
//  Created by James Cash on 14-08-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Language.h"

@interface LanguageCell : UITableViewCell

@property (nonatomic,strong,readonly) Language* language;

- (void)configureCell:(Language*)lang;

@end
