//
//  ViewController.m
//  TableViewsBreakout
//
//  Created by James Cash on 14-08-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"
#import "LanguageCell.h"
#import "Language.h"
#import "AddNewViewController.h"

@interface ViewController () <UITableViewDataSource, AddNewLanguageDelegate>
@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) NSMutableArray *languages;
@end

static NSString* kLanguageCellIdentifier = @"languageCell";

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.languages = [@[[[Language alloc] initWithName:@"Objective-C" paradigm:@"Object-Oriented"],
                        [[Language alloc] initWithName:@"Swift" paradigm:@"functional, object-oriented"]]
                      mutableCopy];

    self.tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.tableView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.tableView];
    self.tableView.dataSource = self;
    [self.tableView registerClass:[LanguageCell class] forCellReuseIdentifier:kLanguageCellIdentifier];

    UIButton *btn = [[UIButton alloc] init];
    btn.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:btn];
    [btn setTitle:@"Add Language" forState:UIControlStateNormal];
    [btn setTitleColor:UIColor.purpleColor forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(goToAddNewItem:) forControlEvents:UIControlEventTouchUpInside];

    [NSLayoutConstraint activateConstraints:
     @[[self.tableView.widthAnchor constraintEqualToAnchor:self.view.widthAnchor],
       [self.tableView.topAnchor constraintEqualToAnchor:self.view.topAnchor],
       [self.tableView.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor],

       [self.tableView.bottomAnchor constraintEqualToAnchor:btn.topAnchor],
       [btn.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor],
       [btn.heightAnchor constraintEqualToConstant:50],
       [btn.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor]
       ]];

}

- (void)goToAddNewItem:(UIButton*)sender
{
    [self performSegueWithIdentifier:@"AddNewSegue" sender:sender];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"AddNewSegue"]) {
        AddNewViewController *dvc = segue.destinationViewController;
        dvc.delegate = self;
    }
}

#pragma mark - AddNewLanguageDelegate

- (void)didAddLanguage:(Language *)language
{
    [self.languages addObject:language];
    [self.tableView reloadData];
}

#pragma mark - UITableViewDatasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.languages.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LanguageCell *cell = [tableView dequeueReusableCellWithIdentifier:kLanguageCellIdentifier forIndexPath:indexPath];

    // configure cell
    Language *language = self.languages[indexPath.row];
    [cell configureCell:language];

    return cell;
}

@end
