//
//  Language.m
//  TableViewsBreakout
//
//  Created by James Cash on 14-08-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "Language.h"

@implementation Language

- (instancetype)initWithName:(NSString *)name paradigm:(NSString *)paradigm
{
    self = [super init];
    if (self) {
        _name = name;
        _paradigm = paradigm;
    }
    return self;
}

@end
