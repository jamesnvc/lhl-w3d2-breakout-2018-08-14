//
//  LanguageCell.m
//  TableViewsBreakout
//
//  Created by James Cash on 14-08-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "LanguageCell.h"

@interface LanguageCell ()
@property (nonatomic,strong) UILabel *nameLabel;
@end

@implementation LanguageCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _nameLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _nameLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:_nameLabel];
        [NSLayoutConstraint activateConstraints:
         @[
           [_nameLabel.centerXAnchor constraintEqualToAnchor:self.contentView.centerXAnchor],
           [_nameLabel.centerYAnchor constraintEqualToAnchor:self.contentView.centerYAnchor],

           [_nameLabel.leadingAnchor constraintEqualToAnchor:self.contentView.leadingAnchor constant:20]
           ]];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configureCell:(Language *)lang
{
    _language = lang;
    self.nameLabel.text = lang.name;
}

@end
