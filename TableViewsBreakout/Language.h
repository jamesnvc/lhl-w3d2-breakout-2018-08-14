//
//  Language.h
//  TableViewsBreakout
//
//  Created by James Cash on 14-08-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Language : NSObject

@property (nonatomic,strong) NSString *name;
@property (nonatomic,strong) NSString *paradigm;

- (instancetype)initWithName:(NSString*)name paradigm:(NSString*)paradigm;

@end
