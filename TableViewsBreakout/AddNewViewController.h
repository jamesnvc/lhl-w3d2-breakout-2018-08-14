//
//  AddNewViewController.h
//  TableViewsBreakout
//
//  Created by James Cash on 14-08-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Language.h"

@protocol AddNewLanguageDelegate
- (void)didAddLanguage:(Language*)language;
@end

@interface AddNewViewController : UIViewController

@property (nonatomic,weak) id<AddNewLanguageDelegate> delegate;

@end
